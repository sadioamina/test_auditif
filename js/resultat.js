var resultat=[];
var num=0;
/* fonction anonyme qui récupère les valeurs avec le song joué correspondant à chaque fois que l'utilisateur déplace le curseur*/
document.getElementById("myRange").addEventListener('change', () => {
	var value = document.getElementById('myRange').value;
	/*alert(value);*/
	if(value < 50) {
		resultat[num]={
			value: value,
			/*Récupération du son joué en local*/
			song: localStorage.getItem('actualSound')
		}
	}
	else {
		resultat[num]={
			value: value,
			song: localStorage.getItem('actualSound')
		}
	}
}, false);



function download(text, name) {
    var a = document.getElementById("a");
    var file = new Blob([text], {type: 'text/plain'});
    /*Déclenche le téléchargement du fichier*/
	a.href = URL.createObjectURL(file);
	a.download = name;
	a.click();
}
