var sonsjouer = [];
/* nombre de test ou d'essai */
var nbtest=0;
/*nombre de son joué pour chaque essai*/
var nbsonjoue=0;
/*contient les données du formulaire*/
var text = '';
/*Fonction qui choisit aléatoirement les nombre et prend en paramètre un min et un max*/
function random(min, max) {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;

}

/*Fonction vérifiant si un son a été joué ou pas */
function contains(arr, element) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === element) {
            return true;
        }
    }
    return false;
}

function audioplay1() {
    var audio_files = ["song/Birds_1.wav", "song/Boire.wav", "song/DistortedGuitarNeckDissonance.wav"];
    
    if (window.sonsjouer.length != audio_files.length) {
        /*Choix d'un nombre aléatoire entre 0 et le nombre maximal de son */
        var randint = audio_files[random(0, audio_files.length)];
        while (contains(window.sonsjouer, randint)) {
            randint = audio_files[random(0, audio_files.length)];

        }
        var audio = new Audio(randint);
       
        /*Lire avec le volume de référence */
        audio.volume = localStorage.getItem('volume_ref') ;
        /*Jouer le son*/
        audio.play();
        actualSound = audio;
        
        window.sonsjouer.push(randint);
        window.num++;
        console.log(window.sonsjouer);
        localStorage.setItem('actualSound', actualSound.src);
        window.nbsonjoue++;
        document.getElementById("sonjouer").innerHTML="<br>Nombre de son joué : <br>"+window.nbsonjoue+"/"+audio_files.length;
       

    } else {
        window.nbtest += 1;
        if (window.nbtest != 3) {
            window.sonsjouer = [];
            alert("essai suivant");
            window.nbsonjoue=0;


            
             
        }else{
            /*Désactiver le bouton 'jouer son' aprés chaque fin d'essai*/
            document.getElementById('bouton1').disabled=true;
            alert("Fin test");
            /*JSON.parse(),méthode pour structurer en string et parser les données*/       
            var patient = JSON.parse(localStorage.getItem('infoPatient'));
            text += 'AGE : ' + patient.age + '\n';
            text += 'Materiel : ' + patient.materiel + '\n';
            text += 'Audition : ' + patient.audition + '\n';
            text += 'Acouphene : ' + patient.acouphene + '\n';
            text += 'Hypersensibilite : ' + patient.hypersensibilite + '\n';
            text += 'Reaction : ' + patient.reaction + '\n';
            text += 'VOLUME : ' + localStorage.getItem('volume_ref') + '\n';

            resultat.forEach((result) => {
                text += result.song.split('/')[result.song.split('/').length -1] + ': ' + result.value + ' % \n';
        })
            /*appel de la fonction download pour récupèrer les résultats sous format texte*/
            download(text, new Date() + '.txt');

            
    }

}}

/* Faire répéter un son d'une manière illimité*/
function repeter() {
    var taille = window.sonsjouer.length;
    var dernierson = window.sonsjouer[taille - 1];
    // alert(dernierson);
    var audio = new Audio(dernierson);

    audio.play();
    actualSound = audio;
}