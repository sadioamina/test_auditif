		DEVELOPPEMENT D'UNE INTERFACE WEB POUR L'EXPORTATION EN LIGNE D'UNE TACHE PSYCHOACOUSTIQUE
		
Cette interface est conçue pour tester en ligne l'audition des personnes.
Des sons leur seront présentés au cours du test.

STRUCTURE DU PROJET

Tous les fichiers sont sauvegardés dans un meme dossier. Il existe des sous dossiers aussi.
Les fichiers sont répartis en fichiers html, css et javascript.

Fichiers HTML : Ces fichiers permettent d'inclure des contenus dans les pages tels des textes, des bouttons, des multimedias...

	1/ index.html : c'est le document html principal qu'on ouve avec le navigateur pour lancer l'interface.
	
	2/ formulaire_patient.html : c'est un formulaire pour l'utilisateur, ce dernier répondra à certaines questions sur son audition.
	
	3/ volume_confortable.html : document html donnant l'utilisateur la possibilité de régler le niveau du volume du son de 		référence ,qui lui convient.
	
	4/ test_html : test principal, incluant toutes les possibilités du test.
	
Sous dossier css : contient tous des fichiers .css

Ils sont tous la meme fonction : donner un style aux diférentes pages ,aux contenus ,tels la couleur, les position des éléments, le 	background, la taille des écritures, etc. 
 
Sous dossier js : ce sont tous des fichiers .js

Ils ont permis l'implémentation de toutes les fonctionnalités à savoir récupération du volume confortable choisi ppar l'utilisateur,jouer les sons d'une manière aléatoire, faire 3 essais, répéter les sons, et récupération des résultats.

 . volume_confortable.js :  récupération du volume confortable avec la fonction getVolume().
 
 . test_audio.js : Lecture  avec randomisation .Il lit d'une manière aléatoire les sons, faire de tel sorte que chaque son puisse etre répéter autant de fois que l'on veut, faire 3 essais (par exemple avec 3 sons dans notre liste de son,nous aurons à jouer 9 sons avec 3 essais), afficher le nombre de sons déjà joués au cours du test. La fonction contains() cherche si un son a été déjà joué ou pas, si oui, elle l'omet,sinon ele le joue: c'est pour éviter la répétition d'un son dans un méme essai.
 
 .resultat.js : récupérer les valeurs du volume du son joué ,récupération des données en local avec localStorage, et enregistrement sous format texte des données avec la fonction download().
 
Les fichiers css et js sont appelés dans les fichiers html pourqu'ils puissent fonctionner.

Sous dossier song : les sons à jouer sont sauvegardés dans ce dossier.
